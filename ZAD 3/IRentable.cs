﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZAD_3
{
    interface IRentable
    {
        String Description { get; }
        double CalculatePrice();
    }
}
