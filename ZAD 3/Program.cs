﻿using System;
using System.Collections.Generic;

namespace ZAD_3
{
    class Program
    {
        static void Main(string[] args)
        {
            List < IRentable > list = new List<IRentable>();
            Book book = new Book("616 nijansi zelene");
            Video video = new Video("Brzi i zestoki 34: Zadnja zadnja zadnja voznja");
            list.Add(book);
            list.Add(video);
            RentingConsolePrinter printer = new RentingConsolePrinter();
            printer.DisplayItems(list);
            printer.PrintTotalPrice(list);
        }
    }
}
