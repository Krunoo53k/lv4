﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZAD_4
{
    interface IRentable
    {
        String Description { get; }
        double CalculatePrice();
    }
}
