﻿using System;
using System.Collections.Generic;

namespace ZAD_4
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IRentable> list = new List<IRentable>();
            Book book = new Book("kNjiga");
            Video video = new Video("Sharknado 2");
            HotItem hotBook = new HotItem(book);
            HotItem hotVideo = new HotItem(video);
            RentingConsolePrinter printer = new RentingConsolePrinter();
            list.Add(hotBook);
            list.Add(hotVideo);
            printer.DisplayItems(list);
            printer.PrintTotalPrice(list);
        }
    }
}
