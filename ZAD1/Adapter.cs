﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZAD1
{
    class Adapter:IAnalytics
    {
        private Analyzer3rdParty analyticsService;

        public Adapter(Analyzer3rdParty service)
        {
            this.analyticsService = service;
        }

        private double[][] ConvertData(Dataset dataset)
        {
            double[][] matrix = new double[dataset.GetData().Count][];
            int i = 0;
            int j;
            foreach(var element in dataset.GetData())
            {
                j = 0;
                matrix[i] = new double[element.Count];
                foreach(double number in element)
                {
                    matrix[i][j] = number;
                    j++;
                }
                i++;
            }
            return matrix;
        }

        public double[] CalculateAveragePerColumn(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerColumnAverage(data);
        }
        public double[] CalculateAveragePerRow(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerRowAverage(data);
        }
    }
}
