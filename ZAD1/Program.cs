﻿using System;

namespace ZAD1
{
    class Program
    {
        static void Main(string[] args)
        {
            Dataset dataset = new Dataset("test.txt");
            Analyzer3rdParty analyzer= new Analyzer3rdParty();
            Adapter adapter = new Adapter(analyzer);
            Console.WriteLine("Drugi zad");
            for(int i=0;i< adapter.CalculateAveragePerColumn(dataset).Length;i++)
            {
                Console.Write(adapter.CalculateAveragePerColumn(dataset)[i]+" ");
            }
            Console.WriteLine();

            for (int i = 0; i < adapter.CalculateAveragePerRow(dataset).Length; i++)
            {
                Console.Write(adapter.CalculateAveragePerRow(dataset)[i] + " ");
            }

        }

        }
    
}
